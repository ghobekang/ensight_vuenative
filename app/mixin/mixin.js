import papas from '../assets/js_/papaparse.min'

var mixin = {
  methods: {
    getFirebaseDocURL() {
      const _self = this;
      let url = "";
      
      const storageRef = this.$firebase.storage().ref();
      let downloadURL;
      if (_self.$store.state.docLevel === 0) {
        downloadURL = "words.csv";
      } else if (_self.$store.state.docLevel === 1) {
        downloadURL = "word2.csv";
      } else if (_self.$store.state.docLevel === 2) {
        downloadURL = "word3.csv";
      }
      const childRef = storageRef.child(downloadURL);

      childRef
        .getDownloadURL()
        .then(theUrl => {
          this.parseCsvFile(theUrl);
        })
        .catch(error => console.log("Download error: " + error));
    },
    parseCsvFile(url) {
      const _self = this;
      papas.parse(url, {
        delimiter: ",",
        download: true,
        complete: function(result) {
          const data = result.data;
          let structuredData = [];

          for (let i = 0; i < data.length; i++) {
            structuredData.push({
              index: data[i][0],
              eng: data[i][1],
              kor: data[i][2] != "" ? data[i][2].split(";") : null,
              flag: data[i][3],
              pron: data[i][4],
              class: data[i][5],
              orikor: data[i][6] != "" ? data[i][6].split(";") : null,
              regexr: _self.getRegexr(data[i][2].split(";"), data[i][3]),
              matchedcount: 0
            });
          }
          _self.$ls.setItemObject("wordDB", structuredData);
          _self.$store.commit("putDataIntoDB", structuredData);
        }
      });
    },
    getRegexr(bunchofWords, flag) {
      if (bunchofWords.length < 1) {
        return false;
      }
      let regkor = [];
      const tailPatterns_i = "(?=[^가-힣])";
      const tailPatterns_a =
        "(?=(는[\\.\\?\\!]|다[\\.\\?\\!]|는다[\\.\\?\\!]|군[\\.\\?\\!]|는군[\\.\\?\\!]|네[\\.\\?\\!]|마[\\.\\?\\!]|걸[\\.\\?\\!]|래[\\.\\?\\!]|라[\\.\\?\\!]|는단다[\\.\\?\\!]|단다[\\.\\?\\!]|란다[\\.\\?\\!]|냐\\?|느냐\\?|니\\?|련\\?|을쏘냐\\?|쏘냐\\?|대\\?|담\\?|려무나[\\.\\?\\!]|렴[\\.\\?\\!]|소서[\\.\\?\\!]|요[\\.\\?\\!]|니다[\\.\\?\\!]|습니다[\\.\\?\\!]|니까[\\.\\?\\!]|습니까[\\.\\?\\!]|겠다[\\.\\?\\!]|겠어[\\.\\?\\!]|겠어요[\\.\\?\\!]|겠습니다[\\.\\?\\!]|서|여서|니|니까|지만|데|더니|고|면|야|여야|시다[\\.\\?\\!]|셔[\\.\\?\\!]|세요[\\.\\?\\!]|셔요[\\.\\?\\!]|십니다[\\.\\?\\!]|셨다[\\.\\?\\!]|셨어[\\.\\?\\!]|셨어요[\\.\\?\\!]|셨습니다[\\.\\?\\!]|시느냐[\\.\\?\\!]|십니까[\\.\\?\\!]|셨느냐[\\.\\?\\!]|셨습니까[\\.\\?\\!]|시겠다[\\.\\?\\!]|시겠어[\\.\\?\\!]|시겠어요[\\.\\?\\!]|시겠습니다[\\.\\?\\!]|셔서|시니|시니까|시지만|신데|시더니|시고|시면|셔야)[^가-힣])(?!,)";
      const tailPatterns_v =
        "(?=(다[\\.\\?\\!]|는다[\\.\\?\\!]|군[\\.\\?\\!]|는군[\\.\\?\\!]|네[\\.\\?\\!]|마[\\.\\?\\!]|걸[\\.\\?\\!]|래[\\.\\?\\!]|라[\\.\\?\\!]|는단다[\\.\\?\\!]|단다[\\.\\?\\!]|란다[\\.\\?\\!]|냐\\?|느냐\\?|니\\?|련\\?|을쏘냐\\?|쏘냐\\?|대\\?|담\\?|려무나[\\.\\?\\!]|렴[\\.\\?\\!]|소서[\\.\\?\\!]|요[\\.\\?\\!]|니다[\\.\\?\\!]|니까[\\.\\?\\!]|자[\\.\\?\\!]|시다[\\.\\?\\!]|여라[\\.\\?\\!]|시오[\\.\\?\\!]|서[\\.\\?\\!]|겠다[\\.\\?\\!]|겠어[\\.\\?\\!]|겠어요[\\.\\?\\!]|겠습니다[\\.\\?\\!]|여서|니|니까|지만|는데|더니|고|면|야|여야|려고|습니다[\\.\\?\\!]|습니까\\?|느냐[\\.\\?\\!]|요[\\.\\?\\!]|신다[\\.\\?\\!]|셔[\\.\\?\\!]|세요[\\.\\?\\!]|셔요[\\.\\?\\!]|십니다[\\.\\?\\!]|셨다[\\.\\?\\!]|셨어[\\.\\?\\!]|셨어요[\\.\\?\\!]|셨습니다[\\.\\?\\!]|시느냐[\\.\\?\\!]|십니까\\?|셨느냐[\\.\\?\\!]|셨습니까\\?|시라[\\.\\?\\!]|십시오[\\.\\?\\!]|시겠다[\\.\\?\\!]|시겠어[\\.\\?\\!]|시겠어요[\\.\\?\\!]|시겠습니다[\\.\\?\\!]|셔서|시니|시니까|시지만|시는데|시더니|시고|시면|셔야|시려고)[^가-힣])(?!,)";
      const tailPatterns_vp =
        "?(?=(다[\\.\\?\\!]|어[\\.\\?\\!]|어요[\\.\\?\\!]|습니다[\\.\\?\\!]|느냐[\\.\\?\\!]|습니까[\\.\\?\\!]|셨다[\\.\\?\\!]|셨어[\\.\\?\\!]|셨어요[\\.\\?\\!]|셨습니다[\\.\\?\\!]|셨느냐[\\.\\?\\!]|셨습니까[\\.\\?\\!]|을라[\\.\\?\\!]|단다[\\.\\?\\!]|을걸[\\.\\?\\!]|군[\\.\\?\\!]|구나[\\.\\?\\!]|란다[\\.\\?\\!]|느냐[\\.\\?\\!]|니[\\.\\?\\!]|련[\\.\\?\\!]|으랴[\\.\\?\\!]|을쏘냐[\\.\\?\\!]|대[\\.\\?\\!]|담[\\.\\?\\!]|으니까|으면서|으면)[^가-힣])(?!,)";
      const tailPatterns_n =
        "(?=(이|가|로써|으로써|에서|에게서|부터|까지|에게|한테|께|와|과|에서|을|를|의|로서|으로서|으로|로)[^가-힣])(?!,)";
      const tailPatterns_d = tailPatterns_i;
      const tailPatterns_t = tailPatterns_v;

      for (let i = 0; i < bunchofWords.length; i++) {
        if (bunchofWords[i] === "") {
          return false;
        }
        switch (flag) {
          case "i": {
            regkor[i] =
              "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_i;
            break;
          }
          case "a": {
            regkor[i] =
              "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_a;
            break;
          }
          case "v": {
            regkor[i] =
              "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_v;
            break;
          }
          case "vp": {
            regkor[i] =
              "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_vp;
            break;
          }
          case "n": {
            regkor[i] =
              "(?:[^가-힣a-zA-Z0-9<>()])" + bunchofWords[i] + tailPatterns_n;
            break;
          }
          case "d": {
            regkor[i] = bunchofWords[i] + tailPatterns_d;
            break;
          }
          case "t": {
            regkor[i] = bunchofWords[i] + tailPatterns_t;
            break;
          }
        }
      }
      return regkor;
    }
  }
};

export default mixin;
