import main from '../components/App.vue'
import detail_contents from '../components/detailContents.vue'
import voca_page from '../components/voca.vue'
import setting_page from '../components/setting.vue'
import writing_page from '../components/precticeWriting.vue'

var router = {
    home : main,
    detail : detail_contents,
    voca : voca_page,
    setting: setting_page,
    writing: writing_page
}

export default router
