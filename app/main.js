import Vue from "nativescript-vue";
import router from "./route/index";
import Vuex from "vuex";
import BottomNavigation from "nativescript-bottom-navigation/vue";
import * as fs from "tns-core-modules/file-system";
import _ from "lodash";
import bottomNav from "./components/bottomNav.vue";
import actionBar from "./components/actionBarConp.vue";
import sidedrawer from "nativescript-ui-sidedrawer/vue";
import mymixin from "./mixin/mixin";
import * as applicationSettings from "tns-core-modules/application-settings";

const ls = require("nativescript-localstorage");

Vue.registerElement(
  "CardView",
  () => require("nativescript-cardview").CardView
);
Vue.component("bottom-nav", bottomNav);
Vue.component("action-bar", actionBar);
Vue.prototype.$router = router;
Vue.prototype.$goto = function(to, options) {
  var options = options || {
    clearHistory: false,
    backstackVisible: false,
    transition: {
      name: "fade",
      duration: 380,
      curve: "easeIn"
    }
  };
  this.$navigateTo(this.$router[to], options);
};

Vue.use(BottomNavigation);
Vue.use(Vuex);
Vue.use(sidedrawer);

Vue.prototype.$ls = ls;
Vue.prototype._ = _;
Vue.prototype.$appsetting = applicationSettings;

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = TNS_ENV === "production";

const store = new Vuex.Store({
  state: {
    docLevel: 0,
    currentTab: 0,
    wordDB: [],
    vocaDB: [],
    knownDB: [],
    mainList: [],
    products: [],
    LIMIT_WORD_COUNT_RATE: 1
  },
  mutations: {
    putDataIntoDB(state, data) {
      state.wordDB = data;
      console.log("data input done");
    },
    putEntireVocaDB(state, data) {
      state.vocaDB = data;
      console.log("voca is totally updated");
    },
    setLimitCount(state, num) {
      state.LIMIT_WORD_COUNT_RATE = num;
    },
    putDataIntoVocaDB(state, data) {
      if (typeof data === 'object') {
        for (let index = 0; index < data.length; index++) {
          state.vocaDB.push(data[index]);
          state.vocaDB = _.uniqWith(state.vocaDB, _.isEqual);
        }
      }
    },
    putDataIntoKnownDB(state, data) {
      if (typeof data === 'object') {
        state.knownDB.push(data);
        _.remove(state.wordDB, data);
        _.remove(state.vocaDB, data);
        state.knownDB = _.uniqWith(state.knownDB, _.isEqual);   
      }
    },
    updateWordCounting(state, index) {
      let updatedObj = _.chain(state.wordDB).find({index: index}).update('matchedcount', function(n) {return n+1}).value()
      
      if (updatedObj.matchedcount > 10) {
        const data = _.find(state.wordDB, { index: index });
        this.commit("putDataIntoKnownDB", data);
      }
    },
    getPersistanceFilePath(state, filename) {
      const doc = fs.knownFolders.documents();
      const path = fs.path.join(doc.path, filename);
      return fs.File.fromPath(path);
    },
    updateDocLevel(state, level) {
      state.docLevel = level;
    },
    updateLimitWords(state, num) {
      state.LIMIT_WORD_COUNT_RATE = num;
    },
    cacheMainPage(state, lists) {
      state.mainList = lists;
    },
    clearMainList(state) {
      state.mainList = [];
    },
    updateCurrentTab(state, num) {
      state.currentTab = num;
    },
    syncPersonalData(state, dataset) {
      const documents = fs.knownFolders.documents();
      const folder = documents.getFolder("userEntity");
      const file = folder.getFile("userinfo.json");

      file.writeTextSync(dataset, err => {
        console.log(err);
      });
    },
    initData(state) {
      state.wordDB = _.chain(state.wordDB).concat(state.knownDB).sortBy(function(o) {return o.index}).value()
      state.knownDB = [];
      state.vocaDB = [];
    }
  },
  actions: {
    syncDBwithLocal(context) {
      ls.setItemObject("wordDB", context.state.wordDB);
      ls.setItemObject("vocaDB", context.state.vocaDB);
      ls.setItemObject("knownDB", context.state.knownDB);
      const settings = {
        level: context.state.docLevel,
        limit: context.state.LIMIT_WORD_COUNT_RATE
      };
      ls.setItemObject("settings", settings);
      const dataset = {
        vocaDB: context.state.vocaDB,
        knownDB: context.state.knownDB,
        setting: settings
      };
      context.commit("syncPersonalData", JSON.stringify(dataset));
    },
    restoreDataset(context) {
      const documents = fs.knownFolders.documents();
      const folder = documents.getFolder("userEntity");
      const file = folder.getFile("userinfo.json");
      if (file.size !== 0) {
        const conq = JSON.parse(file.readTextSync());
        context.state.vocaDB = conq.vocaDB;
        context.state.knownDB = conq.knownDB;
        context.state.docLevel = conq.setting.level;
        context.state.LIMIT_WORD_COUNT = conq.setting.limit;
      }
    }
  }
});

Vue.prototype.$store = store;

new Vue({
  render: h => h("frame", [h(router["home"])]),
  mixins: [mymixin],
  beforeDestroy() {
    this.$store.dispatch("syncDBwithLocal");
  },
  created() {
    const _self = this;

    let wordDB = _self.$ls.getItem("wordDB");
    
    if (!wordDB) wordDB = [];

    this.$store.commit('updateCurrentTab', 0)
    this.$store.dispatch("restoreDataset");
    if (wordDB.length === 0) {
      _self.getFirebaseDocURL();
    } else {
      const VocaDB = _self.$ls.getItem("vocaDB");
      _self.$store.commit("putDataIntoDB", wordDB);

      if (VocaDB) _self.$store.commit("putEntireVocaDB", VocaDB);
    }
  }
}).$start();
